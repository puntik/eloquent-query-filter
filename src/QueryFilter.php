<?php declare(strict_types = 1);

namespace Abetzi\EloquentFilter;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class QueryFilter
{

    /** @var Request */
    protected $request;

    /** @var Builder */
    protected $query;

    protected array $filters;

    public function __construct(Request $request = null)
    {
        if ($request) {
            $this->filters = $request->all();
        }

        $this->filters = [];
    }

    public function apply($query)
    {
        $this->query = $query;

        foreach ($this->filters as $field => $value) {
            $method = Str::camel($field);

            if (method_exists($this, $method)) {
                call_user_func_array([$this, $method], (array)$value);
            }
        }

        return $this->query;
    }

    public static function from(array $filters): self
    {
        return (new static())->setFilters($filters);
    }

    private function setFilters(array $filters): self
    {
        $this->filters = $filters;

        return $this;
    }
}
