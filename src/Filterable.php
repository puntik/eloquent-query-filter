<?php declare(strict_types = 1);

namespace Abetzi\EloquentFilter;

use Illuminate\Database\Eloquent\Builder;

trait Filterable
{

    public function scopeFilter(
        Builder $query,
        QueryFilter $filter
    ): Builder {
        return $filter->apply($query);
    }

}
