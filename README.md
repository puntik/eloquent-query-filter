Some links:

* [QueryFilter: A Model Filtering Concept](https://blog.jgrossi.com/2018/queryfilter-a-model-filtering-concept/)
* [Query Filters (Cerebro90)](https://github.com/cerbero90/query-filters/tree/develop)
* [Query Filters (kblais)](https://github.com/kblais/query-filter)
* [Laracast lesson](https://laracasts.com/series/eloquent-techniques/episodes/4)

