FROM composer:latest AS composer

FROM php:8.0-cli-buster
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

RUN apt-get update \
	&& apt-get install -y \
		zip \
		unzip \
		curl \
	&& rm -rf /var/lib/apt/lists/* \
	&& pecl install xdebug \
	&& docker-php-ext-enable xdebug \
	&& echo "xdebug.mode=debug\n" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
	&& echo "xdebug.idekey=\"PHPSTORM\"\n" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
	&& echo "xdebug.client_port=9003\n" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
	&& echo "xdebug.client_host=host.docker.internal\n" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

ENV COMPOSER_ALLOW_SUPERUSER=1

ENTRYPOINT [ "sleep", "infinity" ]
WORKDIR /var/eloquent-query-filter
