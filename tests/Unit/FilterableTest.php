<?php

namespace Abetzi\EloquentFilter\Test\Unit;

use Abetzi\EloquentFilter\Test\Car;
use Abetzi\EloquentFilter\Test\CarFilter;
use Abetzi\EloquentFilter\Test\TestCase;

class FilterableTest extends TestCase
{

    /**
     * @test
     */
    public function it_works()
    {
        // Given
        $eloquentBuilder = Car::where('color', '=', 'blue')->getQuery();

        // When
        $filterBuilder = Car::filter(CarFilter::from(['color' => 'blue']))->getQuery();

        // Then
        $this->assertSame(
            $filterBuilder->toSql(),
            $eloquentBuilder->toSql()
        );

        $this->assertSame(
            $filterBuilder->getBindings(),
            $eloquentBuilder->getBindings(),
        );
    }

}
