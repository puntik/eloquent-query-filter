<?php declare(strict_types = 1);

namespace Abetzi\EloquentFilter\Test;

use Abetzi\EloquentFilter\QueryFilter;

class CarFilter extends QueryFilter
{

    public function color(string $color)
    {
        $this->query->where('color', '=', $color);
    }
}
