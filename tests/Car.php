<?php declare(strict_types = 1);

namespace Abetzi\EloquentFilter\Test;

use Abetzi\EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{

    use Filterable;
}
